package com.zuitt.example;

//Interface implementation
public class Ferson implements Actions, Greetings{

    @Override
    public void sleep() {
        System.out.println("Zzzzzzzzzz. . . .");
    }

    @Override
    public void run() {
        System.out.println("Running . . ");
    }

    @Override
    public void morningGreet() {
        System.out.println("Good morning!");
    }

    @Override
    public void holidayGreet() {
        System.out.println("Happy Holidays!");
    }
}
